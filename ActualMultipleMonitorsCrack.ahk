;==================================================================
; ACTUAL MULTIPLE MONITORS CRACK v1.0.0
; Author: Daniel Shuy
; 
; A script to bypass the 30-minute per day trial period of Actual Multiple Monitors
;==================================================================

#NoEnv

#Include %A_ScriptDir%\ShellHook-1.0.1.ahk

;==================================================================
; User Variable Configuration
;==================================================================
	; Folder where Actual Multiple Monitors was installed to. Path must end with a '\'.
	FOLDER_PATH := "C:\Program Files (x86)\Actual Multiple Monitors\"
	
	PROCESS_NAME_CORE := "ActualMultipleMonitorsCenter.exe"
	PROCESS_NAME_SHELL := "ActualMultipleMonitorsShellCenter64.exe"
;==================================================================
Gui +LastFound	; Win commands called specifying window will use last found window instead
	
	processPathCore := FOLDER_PATH . PROCESS_NAME_CORE
	winTitle := "ahk_exe " . processPathCore
	
	hWnd := WinExist()	; get reference to this script
	
	; register Shell Hook to detect Window opening/closing event
	shellHook := new ActualMultipleMonitorsShellHook(hWnd, PROCESS_NAME_CORE, PROCESS_NAME_SHELL, processPathCore)
	
	Init()
return

Init() {
global
	if (ProcessExists(PROCESS_NAME_CORE)) {
		; if trial pop-up Window is displayed, bypass it
		local id := WinExist(winTitle)
		if (id) {
			shellHook.BypassTrial(id)
		}
	}
	else {
		; if Actual Multiple Monitors is not yet started, launch it
		Run, %processPathCore%
	}
}

ProcessExists(processName) {
	Process, Exist, %processName%
	return ErrorLevel
}

class ActualMultipleMonitorsShellHook extends ShellHook {
	__New(hWnd, processNameCore, processNameShell, processPathCore) {
		base.__New(hWnd)
		
		this.cmd := "taskkill /IM " . processNameCore
		this.processNameShell := processNameShell
		this.processPathCore := processPathCore
	}
	
	;@Override
	ShellProc(hookCode, id) {
		if (!A_IsPaused) {	; do nothing if script is paused
			if (hookCode = ShellHook.HSHELL_WINDOWCREATED) {
				winTitle := "ahk_id " . id . " ahk_exe " . this.processPathCore
				if WinExist(winTitle) {
					this.BypassTrial(id)
				}
			}
		}
		else {
			; reinitializes script on resume
			SetTimer, Init, -0	; run once immediately. Because script is paused, this will only run when script is resumed
		}
	}
	
	BypassTrial(id) {
		if (ProcessExists(this.processNameShell)) {
			; terminate and relaunch application
			Loop {
				; one taskkill may not be enough. Loop until process no longer exists
				cmd := this.cmd
				RunWait, %cmd%, , Hide
				if (!ProcessExists(this.processNameShell)) {
					break
				}
			}
			processPathCore := this.processPathCore
			Run, %processPathCore%
		}
		else {
			; application just relaunched, close initial pop-up Trial Window pop-up
			WinClose, ahk_id %id%
		}
	}
}

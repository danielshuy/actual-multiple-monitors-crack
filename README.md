# Actual Multiple Monitors Crack

## Synopsis

A for-fun personal AutoHotkey project to attempt to create a "poor man's crack" to bypass the 30-minute per day trial period of Actual Multiple Monitors using the [AutoHotkey ShellHook library](https://gitlab.com/danielshuy/autohotkey-shellhook/)

## Usage

Download the package and extract the contents. Execute **ActualMultipleMonitorsCrack.ahk** with **AutoHotkeyU32.exe**/**AutoHotkeyU64.exe**.

To convert the script to an Executable file (.exe), see https://autohotkey.com/docs/Scripts.htm#ahk2exe, or download **ActualMultipleMonitorsCrack-**_{VERSION}_**.exe**
